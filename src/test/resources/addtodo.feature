Feature: Add Todo
  Scenario: Create todo by POST /todos
    Given ensure rest endpoint of "35.188.162.47:8080" is up
    When a POST request to /todos is made
    And the request body is
      """
{
  "targetDate": "2020-02-28",
  "description": "Test",
  "user": "Suddhasatwa",
  "done": false
}
      """
    Then a 201 response is returned within 2000ms